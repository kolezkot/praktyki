// JavaScript Document

$(document).ready(function() {

  $( function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 1500,
      values: [ 100, 1000 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "$" + $( "#slider-range" ).slider( "values", 0 ) +
      " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  } );
  
  $('.icon').on('click', function(){
		$("ul.menu li").slideToggle();
	});
	
	 $('.f-lewo.categories h2').on('click', function(){
		$(".f-lewo.categories p").slideToggle();
	});
	 $('.f-lewo.price h2').on('click', function(){
		$(".f-lewo.price p, #slider-range").slideToggle();
	});
	 $('.f-lewo.brands h2').on('click', function(){
		$(".f-lewo.brands p").slideToggle();
	});
	 $('.f-lewo.sizes h2').on('click', function(){
		$(".f-lewo.sizes p").slideToggle();
	});
	
	$('.menu-right li:nth-child(3)').on('click', function(){
		$(".menu-right li:nth-child(1)").slideToggle();
	});


});