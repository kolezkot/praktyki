// JavaScript Document

$(document).ready(function() {
    
	$('nav .menu .sub-menu a').each(function(index, element) {
        
		var $this = $(this);
		$this.parent().addClass($this.attr('title'));
		
		if($this.attr('title') == 'all') {
			$this.append($('<span/>'));	
		}

    });
	
	$('nav .menu .sub-menu a').last().css('border-bottom', '0')
	
	//header slider
	
	$('#header.home-slider .slides').carouFredSel({
		
		scroll: {
			items: 1,
			fx: 'fade'
		},
		
		pagination: {
			container: $('#header.home-slider .pagination ul'),
			anchorBuilder: function(nr) {
				return '<li><a href="#">'+nr+'</a></li>';
			}
		}
		
	});
	
	// slider artykuły 
	
	$('#home .inspirations .slider .slides-container .items').carouFredSel({
	
		prev: $('#home .inspirations .slider .prev'),
		next: $('#home .inspirations .slider .next'),
		scroll: {
			items: 1
			}
	
	});
	
	// animacja strzałek
	
	var $sliderNav = $('#home .inspirations .slider > a');
	
	$sliderNav.each(function() {
		var $this = $(this);
		
		if($this.hasClass('prev')) {
			var cleft = parseInt($this.css('left'));
			$this.css('left', (cleft+2));
			var onOver = {left: cleft+'px'};
			var onOut = {left: (cleft+2)+'px'};
		} else {
			var cright = parseInt($this.css('right'));
			$this.css('right', (cright+2));
			var onOver = {right: cright+'px'};
			var onOut = {right: (cright+2)+'px'};
		}
		
		$this.hover(function() {
			$this.animate(onOver, 100);
			}, function() {
				$this.animate(onOut, 100);
				});
        
    });
	
	
	
	
	$('#home .boxes .box').mouseover(function(){
		var $this = $(this);
		
		if($this.find('.step2').size() >0) {
			$this.find('.step1').hide();
			$this.find('.step2').show();
		}
	});
	
	$('#home .boxes .box').mouseout(function(){
		var $this = $(this);
		
		if($this.find('.step2').size() >0) {
			$this.find('.step2').hide();
			$this.find('.step1').show();
		}
	});
	
	//google maps script
	
	var $headerMapCont = $('#header #map');
	
	if($headerMapCont.size() > 0) {
		
		var restaurantsList = [
			{
		  "city":"Krak\u00f3w",
		  "restaurantsCount":"2"
		 	},
		 	{
		  "city":"Warszawa",
		  "restaurantsCount":"1"
		 	},
		 	{
		  "city":"Wroc\u0142aw",
		  "restaurantsCount":"10"
		  }
		];
	
	function initHeaderMap() {
	
		//tworzenie mapki
		function createHeaderMap($cnt, lat, lng) {
			var opts = {
				center: new google.maps.LatLng(lat, lng),
				zoom: 8,
				scrollwheel: false,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			
			var headerMap = new google.maps.Map($cnt.get(0), opts);
			
			if(restaurantsList) {
			
				for(var entry in restaurantsList) {
					
					(function(entry, map) {				
					var geocoder = new google.maps.Geocoder();
					
					geocoder.geocode({
						address: entry.city
					}, function(results, status) {
							if(status == google.maps.GeocoderStatus.OK) {
								var options = {
									strokeColor: "#000",
									strokeOpacity: 1,
									strokeWeight: 2,
									fillColor: "#f55b0c",
									fillOpacity: 1,
									center: results[0].geometry.location,
									map: headerMap,
									radius: entry.restaurantsCount * 3500
								}
								
								new google.maps.Circle(options);
								
							} else {
								alert("Geolokacja nie powiodła się z powodu: " + status);
							}						
					});
					})(restaurantsList[entry], headerMap)
				}
			}
		}
		
		
		
		//sprawdzanie geolokalizacji użytkownika
		if(navigator.geolocation) {
			var success = function(position) {
				createHeaderMap($headerMapCont, position.coords.latitude, position.coords.longitude)
			};
			
			var error = function() {
				createHeaderMap($headerMapCont, 52.259, 21.020); //warsaw coords	
			}
			
			navigator.geolocation.getCurrentPosition(success, error);			
		} else {
			createHeaderMap($headerMapCont, 52.259, 21.020); //warsaw coords	
		}
	}
	
	
	//ładujemy mapę
	google.maps.event.addDomListener(window, 'load', initHeaderMap)
		
		
	}
	
	
	
	
	
	
	
		
	
	
});