// JavaScript Document

	// slider artykuły 
$(document).ready(function() {	
	$('#home .inspirations .slider .slides-container .items').carouFredSel({
	
		prev: $('#home .inspirations .slider .prev'),
		next: $('#home .inspirations .slider .next'),
		scroll: {
			items: 1
			}
	
	});
	
	// animacja strzałek
	
	var $sliderNav = $('#home .inspirations .slider > a');
	
	$sliderNav.each(function() {
		var $this = $(this);
		
		if($this.hasClass('prev')) {
			var cleft = parseInt($this.css('left'));
			$this.css('left', (cleft+2));
			var onOver = {left: cleft+'px'};
			var onOut = {left: (cleft+2)+'px'};
		} else {
			var cright = parseInt($this.css('right'));
			$this.css('right', (cright+2));
			var onOver = {right: cright+'px'};
			var onOut = {right: (cright+2)+'px'};
		}
		
		$this.hover(function() {
			$this.animate(onOver, 100);
			}, function() {
				$this.animate(onOut, 100);
				});
        
    });
	
	
});
	


